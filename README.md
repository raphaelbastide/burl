# Burl

A game you play in the URL

![Gif demo of Burl](img/burl-demo.gif)

## Modify

Burl is a game that can be played, but modification is also part of the fun. You can create and share your own levels, and add more bonuses and platforms.

### Create your own level

- Fork this project and add a new `json` file in the `levels/` directory.
- You can copy the content of `levels/default.json` as a starter
- You can change the positions (`posX`,`posY`) of the bonuses and if want and can, create new [bonuses and platforms](#create-new-platform-types)
- Add your level in `levels/level-list.json` by adding its `name` and `file`
- Test your level
- Create a pull request to make your level available to the players

### Create new platform types

(soon)

## Thanks

- Physics engine: [Matter.js](http://brm.io/matter-js/)
- Font: [Version Skeletor](http://www.xavierklein.eu/versions/), by Xavier Klein

# License

[GNU General Public License](https://www.gnu.org/licenses/gpl.html)
