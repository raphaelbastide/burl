var d = document,
    infobox = d.getElementById("infobox"),
    inventory = d.getElementById('inventory'),
    body = d.getElementsByTagName("body")[0],
    main = d.getElementsByTagName("main")[0]

function preloadImgs() {
  var images = ['img/burl-ball.svg','img/burl-bonus.svg'];
  for (var i = 0; i < arguments.length; i++) {
    images[i] = new Image();
    images[i].src = preload.arguments[i];
  }
}
preloadImgs()

function listLevels(){
  var levelList
  var request = new XMLHttpRequest();
  request.open('GET', 'levels/level-list.json');
  request.onload = function() {
    if (request.status >= 200 && request.status < 400) {
      levelList = JSON.parse(request.responseText);
      var levels = levelList.levels
      for (var i = 0; i < levels.length; i++) {
        var file = "levels/"+levels[i].file
        var line = d.createElement("p")
        line.setAttribute('data-file', file)
        line.setAttribute('class', 'level-link')
        line.setAttribute('onclick', 'selectLevel("'+file+'",true)')
        line.innerHTML = levels[i].name
        d.getElementById('level-list').appendChild(line)
      }
    }else{
      console.log('json not found');
    }
  };
  request.onerror = function() {
    console.log('json request error');
  };
  request.send();
}
listLevels()


function getDistances(unit){
  var realW = unit.bounds.max.x - unit.bounds.min.x
  var realH = unit.bounds.max.y - unit.bounds.min.y
  return unit.distances = {w:realW, h:realH}
}
function checkLock(unit){
  if (unit.isLocked === true) {
    return true
  }else {
    return false
  }
}
function promptInfo(message){
  var infoLine = d.createElement('p')
  infoLine.innerHTML = "→ "+message
  infobox.appendChild(infoLine)
  infoLine.classList.add('visible')
  setTimeout(function(){infoLine.classList.remove('visible')},5000)
  infoLine.classList.add('infoline')
}

function findObjectsByKey(array, key, value) {
  var result = []
  for (var i = 0; i < array.length; i++) {
    if (array[i][key] === value) {
      result.push(array[i]);
    }
  }
  return result
}

function getAvailableChar(data, role=false){
  var availableChar = []
  var allUnits = data.units
  for (var i = 0; i < allUnits.length; i++) {
    if (role) {
      if (allUnits[i].role == role) {
        var c = allUnits[i].char
        availableChar.push(c)
      }
    }else {
      var c = allUnits[i].char
      availableChar.push(c)
    }
  }
  return availableChar
}

function makeInventory(data){
  var availableChar = getAvailableChar(data,'platform')
  var buildZones = getAvailableChar(data,'buildZone')
  for (var i = 0; i < buildZones.length; i++) {
    availableChar.push(buildZones[i]) // adds buildZone to the char list
  }
  inventory.innerHTML = "<p>Inventory</p>" // first, empty (almost all) existing content
  for (var i = 0; i < availableChar.length; i++) {
    var p = d.createElement('p')
    var char = availableChar[i]
    var unit = findObjectsByKey(data.units, 'char', char)[0]
    p.innerHTML = "<span title='"+unit.name+"' class='char'>"+char+"</span> <span title='"+unit.name+"' class='icon "+unit.name+"'></span>"
    p.setAttribute('data-char',char)
    if (unit.isLocked) {
      p.classList.add('locked')
    }
    inventory.appendChild(p)
  }
}
function updateInventory(charToUnlock){
  var line = d.querySelectorAll('#inventory [data-char='+charToUnlock+']')[0]
  line.classList.remove('locked')
  inventory.classList.add('updated')
  setTimeout(function(){
    inventory.classList.remove('updated')
  },100)
}

function resetInfo(){
  var infobox = d.getElementById('infobox')
}

function testHash(data){
  var cleanHash = []
  var availableChar = getAvailableChar(data)
  var hash = window.location.hash.substr(1)
  // Extracting URL characters to build the hash
  if(hash == "#" || hash == "") {
    promptInfo("No terrain yet. Try adding <a href='#dff'>#dff</a> at the end of this URL")
    return false
  }else{
    hashChar = hash.split('')
    for (var i = 0; i < hashChar.length; i++) {
      if (availableChar.indexOf(hashChar[i]) <= -1) { // incompatible character
        promptInfo("<span class='char'>"+hashChar[i]+"</span> is not a valid character")
      }else {
        cleanHash.push(hashChar[i])
      }
    }
    return cleanHash
  }
}
