var data
var request = new XMLHttpRequest();
var defaultLevel = 'levels/default.json'

if (!Cookies.get('level')) { // if no level selected
  selectLevel(defaultLevel, false)
  console.log('No level selected => default level');
}else {
  selectLevel(Cookies.get('level'), false)
  console.log('Level '+Cookies.get('level')+' selected');
}

function selectLevel(level, isUpdate){
  Cookies.set('level', level, { expires: 1 });
  // Json request
  request.open('GET', level);
  request.onload = function() {
    if (request.status >= 200 && request.status < 400) {
      data = JSON.parse(request.responseText);
      console.log('request success');
      launch(data, isUpdate)
      d.getElementById('levelbox').removeAttribute('open')
    }else{
      console.log('json not found');
    }
  };
  request.onerror = function() {
    console.log('json request error');
  };
  request.send();
}
