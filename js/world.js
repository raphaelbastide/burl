
function launch(data, isRefresh){
  var Engine = Matter.Engine,
      Render = Matter.Render,
      Runner = Matter.Runner,
      Composite = Matter.Composite,
      Composites = Matter.Composites,
      Constraint = Matter.Constraint,
      World = Matter.World,
      Bodies = Matter.Bodies,
      Body = Matter.Body,
      Events = Matter.Events;
      var debug = false,
      main = d.getElementsByTagName('main')[0],
      pauseBtn = d.querySelectorAll(".pause-btn"),
      startX = 100,
      startY = 300,
      nextPos = 0,
      lastDir = 0,
      canvasW = 1500,
      canvasH = 750,
      strokeWidth = 5,
      buildW = 400,
      buildH = 30,
      realBuildW = buildW + strokeWidth * 2,
      realBuildH = buildH + strokeWidth * 2,
      ballColor = "#ec82be",
      zoom = false,
      timeScaleTarget = 1,
      zoom = 500,
      zoomTarget = 500,
      bonusList = [],
      bonusCompasList = [],
      counterBonus = counterTime = counterZoom = collisionBuffer = 0,
      bulletTime, bonusTime, isTeleported, checkPoint, lastCollided = false

  if (debug) {
    body.classList.add('debug')
  }
  if (isRefresh) { // removes canvas when hashchange
    var c = d.getElementsByTagName('canvas')[0]
    c.parentElement.removeChild(c)
    body.classList.remove('paused')
    resetInfo()
  }
  // Listen to the URL hash change
  window.onhashchange = locationHashChanged;
  function locationHashChanged() {
    launch(data, true)
  }


  // Create the character List
  makeInventory(data)

  // create engine
  var engine = Engine.create(),
      world = engine.world;
      engine.world.gravity.y = 1,
      engine.enableSleeping = true
  var render = Render.create({
    element: main,
    engine:engine,
    options: {
      wireframes:false,
      showSleeping:false,
      width: canvasW,
      height: canvasH,
      background: data.theme[0].background
    }
  });

  Render.run(render);
  var runner = Runner.create();
  runner.isFixed = true
  Runner.run(runner, engine);

  var hash = testHash(data)
  if (hash) {
    for (var i = 0; i < hash.length; i++) {
      c = hash[i]
      if (!nextPos){nextPos = {x:startX,y:startY}}
      buildUnit(c, nextPos)
    }
  }

  // Building common objects
  var ball = Bodies.circle(100, 100, 100, { friction:1, frictionAir:0.000001, restitution: 0.4})
  ball.render.sprite.texture = 'img/burl-ball.svg'
  var bonuses = findObjectsByKey(data.units, 'role', 'bonus')
  for (var i = 0; i < bonuses.length; i++) {
    var bonus = bonuses[i]
    var toBuild = Bodies.circle(bonus.posX, bonus.posY, 20,{isStatic:true, isSensor:true})
    var bonusCompas = Bodies.circle(-1000, -1000, 3,{isStatic:true, isSensor:true})
    var toUnLock = findObjectsByKey(data.units, 'name', bonus.unlocks)[0]
    var bonusColor = toUnLock.color
    // toUnLock.isLocked = false
    toBuild.render.fillStyle = bonusColor
    toBuild.render.lineWidth = 5
    toBuild.render.strokeStyle = "black"
    toBuild.name = bonus.name
    toBuild.role = bonus.role
    toBuild.unlocks = bonus.unlocks
    bonusCompas.targets = bonus.name
    bonusList.push(toBuild)
    bonusCompasList.push(bonusCompas)
    World.add(world,[toBuild, bonusCompas]);
  }
  //  Adding common objects to the world

  var posY = nextPos.y

  // Building terrain from the character array
  function buildUnit(char, nextPos){
    var unit = findObjectsByKey(data.units, 'char', char)[0];
    if (unit.role === "buildZone") {
      nextPos.x += 0
      nextPos.y += unit.verticalShift
    }else{
      if (unit.verticalShift == 1) {
        var posX = nextPos.x
        var posY = nextPos.y + (unit.angle * 550)
      }else if (unit.verticalShift == 0) {
        var posX = nextPos.x + 35
        var posY = nextPos.y + (unit.angle * 550)
      }else if (unit.verticalShift == -1) {
        var posX = nextPos.x
        var posY = nextPos.y + (unit.angle * 550)
      }
      buildW += buildW * Math.round(Math.abs(unit.angle))
      var toBuild = Bodies.rectangle(posX, posY, buildW - strokeWidth*2, buildH - strokeWidth*2, {isStatic:unit.isStatic,angle: Math.PI * unit.angle})
      if (debug) {
        toBuild.isLocked = false
      }else {
        toBuild.isLocked = unit.isLocked
      }
      toBuild.name = unit.name
      if (checkLock(unit) === false || debug) { // if unlocked
        toBuild.isSensor = unit.isSensor
        toBuild.render.fillStyle = unit.color
        toBuild.render.lineWidth = strokeWidth
      }else{ // if locked
        toBuild.isSensor = true
        toBuild.render.lineWidth = 1
        toBuild.render.fillStyle = "#ffffff00"
      }
      if (toBuild.name === "gap") {
        toBuild.render.strokeStyle = "#ffffff00"
      }else {
        toBuild.render.strokeStyle = "black"
      }
      getDistances(toBuild)
      if (toBuild != null) {
        World.add(world,toBuild);
      }
      if (unit.verticalShift == 1) {
        nextPos.x += Math.round(toBuild.distances.w)
        nextPos.y -= Math.round(toBuild.distances.h) - realBuildH / 2
      }else if (unit.verticalShift == 0) {
        nextPos.x += Math.round(toBuild.distances.w)
        nextPos.y += Math.round(toBuild.distances.h) - realBuildH / 2
      }else {
        nextPos.x += Math.round(toBuild.distances.w)
        nextPos.y += Math.round(toBuild.distances.h) - realBuildH / 2 + 10
      }
    }
  }

  var allBodies = Composite.allBodies(world)
  function unlockFromBonus(bonus){
    var toUnLock  = findObjectsByKey(data.units, 'name', bonus.unlocks)[0]
    for (var i = 0; i < allBodies.length; i++) {
      var unit = allBodies[i]
      if (unit.name == toUnLock.name) {
        unit.isLocked = false
        unit.isSensor = toUnLock.isSensor
        unit.render.fillStyle = toUnLock.color
        unit.render.lineWidth = strokeWidth
      }
      if (unit.targets == bonus.name) {
        World.remove(world, unit)
      }
    }

    World.remove(world, bonus)
    var message = '<span class="char">'+toUnLock.char+'</span> '+toUnLock.name+' unlocked!<br>'
    updateInventory(toUnLock.char)
    promptInfo(message)
  }

  // Render after each Tick
  Events.on(engine, 'afterTick', (e)=> {
    var ballCenter = ball.position
    for (var i = 0; i < bonusList.length; i++) {
      var bonusCenter = bonusList[i].position
      var diffX = bonusCenter.x - ballCenter.x
      var diffY = bonusCenter.y - ballCenter.y
      var angle = Math.atan2(diffY, diffX)
      var compasX = ballCenter.x + 140 * Math.cos(angle)
      var compasY = ballCenter.y + 140 * Math.sin(angle)
      for (var j = 0; j < bonusCompasList.length; j++) {
        if (bonusCompasList[j].targets == bonusList[i].name) {
          Body.setPosition(bonusCompasList[j],{x:compasX, y:compasY})
        }
      }
    }
    if (collisionBuffer >= 8000) {
      togglePause()
      collisionBuffer = 0
    }else {
      collisionBuffer +=1
    }
    if (bulletTime) {
      engine.timing.timeScale = .5
      zoom += (zoomTarget - zoom) * 0.05;
      if (counterZoom >= 60 * 3.5) {
        zoomTarget = 500
        timeScaleTarget = 1
      }else{
        zoomTarget = 200
        timeScaleTarget = 0.05
      }
      if (counterTime >= 60 * 4.5) {
        ball.render.sprite.texture = 'img/burl-ball.svg'
        engine.timing.timeScale = 1
        timeScaleTarget = 1
        zoomTarget = 500
        zoom = 500
        counterTime = 0
        bulletTime = false
      }
      counterTime +=1
    }
    if (bonusTime) {
      if (counterBonus >=20) {
        ball.render.sprite.texture = 'img/burl-ball.svg'
        counterBonus = 0
        bonusTime = false
      }
      counterBonus +=1
    }
    Render.lookAt(render, ball, {x: zoom, y: zoom },true );
  })

  // Handling collisions
  Events.on(engine, 'collisionStart', function(event) {
    collisionBuffer = 0
    var pairs = event.pairs;
    for (var i = 0, j = pairs.length; i != j; ++i) {
      var pair = pairs[i]
      var b1 = pair.bodyB
      var b2 = pair.bodyA // can differ accordins to World.add order
      var col = b2.render.fillStyle
      if (!b2.isLocked) {
        if (b2.name === 'boost' && !b1.inactiveBuffer) {
          b1.render.fillStyle = '#00FF00';
          Body.setVelocity(ball, { x: 40, y: 0 });
          // b1.inactiveBuffer = true
        }else if(b2.name === 'bulletTime') {
          ball.render.sprite.texture = 'img/burl-bullettime.svg'
          bulletTime = true
        }else if(b1.role === 'bonus') {
          bonusTime = true
          var bonus = b1
          ball.render.sprite.texture ="img/burl-bonus.svg"
          unlockFromBonus(bonus)
        }else if(b2.name === 'jump') {
          Body.setVelocity(ball, { x: ball.speed, y: -30 });
        }else if(b2.name === 'invertedGravity') {
          if (engine.world.gravity.y == -1) {
            engine.world.gravity.y = 1;
          }else {
            engine.world.gravity.y = -1;
          }
        }else if(b2.name === 'teleport') {
          var currentCollided = b2.id; // avoid repeated collision
          if ( lastCollided != currentCollided) {
            if (isTeleported) {
              Body.setPosition(ball,{x:checkPoint.x, y:checkPoint.y})
              isTeleported = false
            }else {
              checkPoint = {x:ball.position.x, y:ball.position.y}
              isTeleported = true
            }
            lastCollided = currentCollided
          }
        }
      }
    }
  });
  World.add(world, [ball]);

  for (var i = 0; i < pauseBtn.length; i++) {
    pauseBtn[i].addEventListener('click',function(){
      togglePause()
    })
  }

  function togglePause(){
    if (body.classList.contains('paused')) {
      body.classList.remove('paused')
      Render.run(render);
      Runner.run(runner, engine);      // Render.run(engine)
    }else {
      body.classList.add('paused')
      Render.stop(render);
      Runner.stop(runner, engine);
    }
  }
} // end launch()
